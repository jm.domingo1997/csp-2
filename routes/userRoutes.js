
//directive 
const express = require('express');

//Roouter() handles the request
const router = express.Router();

//import authentification verify
const auth = require('./../auth.js');

//import userController
const userController = require('./../controllers/userControllers');

//User Registration
router.post('/register', (req, res) => {
	userController.register(req.body).then( result => res.send(result));
})
//login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})

//check out
router.post("/checkout", auth.verify, (req, res) => {
	const {isAdmin, id} = auth.decode(req.headers.authorization);
	//console.log(id);
	//console.log(isAdmin);
	if(isAdmin == true){
		return res.send(false);
	}else{
		userController.checkOut(id,req.body).then(resultFromController => res.send(resultFromController));
	}
})

//Get Orders
router.get("/myOrders", auth.verify, (req, res) => {

	const {isAdmin, id} = auth.decode(req.headers.authorization);
	//console.log(id);
	//console.log(isAdmin);
	if(isAdmin == true){
		return res.send(false);
	}else{
		userController.getOrders(id).then(resultFromController => res.send(resultFromController));
	}
})

//Get All orders
router.get("/getAllOrders", auth.verify, (req, res) => {
	const {isAdmin} = auth.decode(req.headers.authorization);
	//console.log(id);
	//console.log(isAdmin);
	if(isAdmin == true){
		userController.getAllOrders().then(resultFromController => res.send(resultFromController));
	}else{
		return res.send(false);
	}
})
//Set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {
	const {isAdmin} = auth.decode(req.headers.authorization);
	const userId = req.params.userId;
	//console.log(id);
	//console.log(isAdmin);
	if(isAdmin == true){
		userController.setAsAdmin(userId).then(resultFromController => res.send(resultFromController));
	}else{
		return res.send(false);
	}
})
//userRoutes export
module.exports = router;